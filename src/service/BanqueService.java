package service;

import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import javax.jws.*;
import Metier.Compte;

@WebService(name = "BanqueWs")
public class BanqueService {
	@WebMethod(operationName = "ConversionEuroToDh")
	public double conversion(@WebParam(name = "Montant") double mt) {
		return mt * 11.3;
	}

	@WebMethod
	public Compte getCompte(@WebParam(name = "code") Long code) {
		return new Compte(code, Math.random() * 8000, new Date());
	}



}
